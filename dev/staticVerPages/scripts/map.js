// init any variables
var location;
var startLocation;
var map;
var running = false;
var t,
    seconds = 0,
    minutes = 0,
    hours = 0,
    t;

// init map
  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: { lat: 51.8581438, lng: -2.2518047 }
  });
}

// Add marker
function addMarker(coords) {
    var marker = new google.maps.Marker({
      position: coords,
      animation: google.maps.Animation.DROP,
      map: map
    });
}

// get users location
function getLocation() {
  // settings
  var geoOptions = {
    timeout: 10 * 1000
  };
  // Success callback
  function geoSuccess(position) {
    var pos = position;
    var posLocation = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
    addMarker(posLocation);
    map.panTo(posLocation);

  }
  // Error callback
  function geoError() {
    console.log('Error: ' + error.code);
  }
  // get the position, return it
  var position = navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
  return position;
}

// timer function
function startTimer() {

  t = setTimeout(timeCount, 1000);
}

function timeCount() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }

    h1.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

    timer();
}
function timer() {
    t = setTimeout(add, 1000);
}


// Start cycle button event
$('#startCycle').on('click', function() {
  console.log("button clicked");
  startLocation = getLocation();
  $('.logo-popup').fadeOut(500);
  $('.cycle-ui').transition({ y: '0px'}, 750, 'ease');
  $('.cycle-ui').toggleClass('showing');
});

// Minimize/Maximize cycle ui event
$('#view-changer').on('click', function() {
  if ($('.cycle-ui').is('.showing')) {
    $('.cycle-ui').transition({ y: '145px'}, 750, 'ease');
    $('.cycle-ui').toggleClass('showing');
  } else {
    $('.cycle-ui').transition({ y: '0px'}, 750, 'ease');
    $('.cycle-ui').toggleClass('showing');
  }
});

// Start cycle event
$('#toggleCycle').on('click', function() {
  console.log("event started");
  if (running === false) {
    // set running to true
    running = true;
    console.log("if true running");
    // Start timer
    setTimeout(startTimer, 1000);
    // get location
  } else if (running === true) {
    console.log("if false");
  }

  // check if the person has started the function
  // if yes
    // set running var to false
    // Stop the timer and stop getting location
    // Add the time taken to the bottom of the array
    // Add the date and the user id to the bottom of the array as well
    // send array with ajax to server and store in a json file
    // change button text back to start
  // if not
    // set running var to true
    // start timer
      // update time within the cycle ui
    // get location on set interval
      // make a sub procedure that runs getLocation and adds to an array
      // store in variable and array.push the var
    // change button text to finish/stop

});
