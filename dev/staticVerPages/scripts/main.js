$(document).ready(function() {
  // set overlay size
  var viewHeight = $(window).height();
  var viewWidth = $(window).width();
  $('.overlay').width(viewWidth).height(viewHeight);
  // reset maps height
  var mapheight = viewHeight - 56;
  $('.map').height(mapheight);
  // hide popup
  $('#giveLocation').on('click', function() {
    $('.popup').fadeOut(500);
    $('.overlay').fadeOut(500);
    // fade in bulb
    $('.logo-popup').fadeIn(500);
  });
});
