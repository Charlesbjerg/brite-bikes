// requires
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// define the user model schema
var userSchema = mongoose.Schema({
  local : {
        username     : String,
        password     : String
  },
  facebook: {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }
});

// Methods
// generate hash for passwords
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
// check if password is valid
userSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.local.password);
};

// create model for users and expose to app
module.exports = mongoose.model('User', userSchema);
