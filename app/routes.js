module.exports = function(app, passport) {

  // Home page
  app.get('/', isLoggedIn, function(req, res) {
    res.render('index.ejs', {
      user: req.user
    });
  });

  // Login page
  app.get('/login', function(req, res) {
    res.render('login.ejs', { message: req.flash('loginMessage')});
  });

  // Login processing
    app.post('/login', passport.authenticate('local-login', {
      successRedirect: '/',
      failureRedirect: '/login',
      failureFlash: true
    }));

  // Signup page
  app.get('/signup', function(req, res) {
    res.render('signup.ejs', { message: req.flash('signupMessage')});
  });

  // Signup processing
    app.post('/signup', passport.authenticate('local-signup',  {
      successRedirect : '/',
      failureRedirect : '/signup',
      failureFlash : true
    }));

  // Profile page
  app.get('/profile', isLoggedIn, function(req, res) {
    res.render('profile.ejs', {
      user: req.user
    });
  });

  // Logout page
  app.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
  });

  // Cycle page
  app.get('/cycle', function(req, res) {
    res.render('cycle.ejs');
  });

  // Dangers page
  app.get('/dangers', function(req, res) {
    res.render('dangers.ejs');
  });

  // Trending page
  app.get('/rides', function(req, res) {
    res.render('rides.ejs');
  });

  // Friends page
  app.get('/chat', function(req, res) {
    res.render('chat.ejs', {
      user: req.user
    });
  });

  // BriteSites page
  app.get('/briteSites', function(req, res) {
    res.render('briteSites.ejs');
  });

  // Maps development page
  app.get('/maps', function(req, res) {
    res.render('maps.ejs');
  });

};

// route middleware to make sure user is logged in
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect('/login');
  }
}
