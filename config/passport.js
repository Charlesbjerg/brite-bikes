// requires
var LocalStrategy = require('passport-local').Strategy;
// require user model
var User = require('../app/models/users.js');

// expose this function to the app
module.exports = function(passport) {
  // - Passport session setup
  // required for persistant login sessions

  // serialize the user for the session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  // deserialize user
  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  // - Local signup
  passport.use('local-signup', new LocalStrategy({
    // default startegy uses username, override with email
    usernameField : 'username',
    passwordField : 'password',
    passReqToCallback : true
  },
  function(req, username, password, done) {
    process.nextTick(function() {
      // find user whose email = form email
      User.findOne({ 'local.username' : username}, function(err, user) {
          // error check
          if (err) {
            return done(err);
          }
          // check if acc with email already exists
          if (user) {
            return done(null, false, req.flash('signupMessage', 'That username is already taken.'));
          } else {
            // if no user with email - create new user
            var newUser = new User();
            // set users local creds
            newUser.local.username = username;
            newUser.local.password = newUser.generateHash(password);
            // save user
            newUser.save(function(err) {
              if (err) {
                throw err;
              } else {
              return done(null, newUser);
            }
            });
          }
      });
    });
  }));
  // - Local login
  passport.use('local-login', new LocalStrategy({
      usernameField: 'username',
      passwordField: 'password',
      passReqToCallback: true
  },
  function (req, username, password, done) {
    User.findOne({ 'local.username' : username}, function(err, user) {
      // error check
      if (err) {
        return done(err);
      }
      // if no user found
      if (!user) {
        return done(null, false, req.flash('loginMessage', 'No user found'));
      }
      // if user found but password incorrect
      if (!user.validPassword(password)) {
        return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
      }
      // if no errors
      return done(null, user);
    });
  }));
};
