// Requiress
var express = require('express');
var bodyParser = require('body-parser');

// Express init
var app = express();
var port = process.env.PORT || 3000;

// Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(__dirname + "/public"));

// Post request handler
app.post('/login', function(req, res) {
  var username=req.body.username;
  var password=req.body.password;
  console.log("User name = "+username+", password is "+password);
  res.end("This should probably send you somewhere tbh");
});


// Express port set
app.listen(port, "0.0.0.0", function() {
  console.log("Server running on %s, ctrl+c to close..", port);
});
module.exports = app;
