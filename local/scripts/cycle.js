// init map
function initMap() {
  var coords = { lat: 51.8584138, lng: -2.2518047 };
  var map = new google.maps.Map(document.getElementById('map'),
  {
    zoom: 15,
    center: coords
  });
  var marker = new google.maps.Marker({
    position: coords,
    map: map
  });
}

// find location
  // get location store in vars as object
  // create new marker and pass through location coords object
  // marker needs to be inside of a success callback function
  // set geo options outside function as they will stay the same
  var geoOptions = {
    timeout: 10 * 1000
  }

function getLocation() {
  // successful callback
  function geoSuccess(position) {
    // set location to object
    var pos = position;
    var location = { lat: pos.coords.latitude, lng: pos.coords.longitude };
    // Refresh map and add marker
    var map = new google.maps.Map(document.getElementById('map'),
    {
      zoom: 15,
      center: location
    });
    var locationMarker = new google.maps.Marker({
      position: location,
      map: map
    });
    // Close popup and overlay
    $('.popup').fadeOut(500);
    $('.overlay').fadeOut(500);
    // fade in bulb
    $('.logo-popup').fadeIn(500);
  }
  // Error callback
  function geoError() {
    console.log('Error: ' + error.code);
  }
  // Get the location
  navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
}
