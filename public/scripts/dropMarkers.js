var markerList = [{
  "hazardType": "Pothole",
  "lat": 51.862819,
  "lng": -2.237757,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.861248,
  "lng": -2.250484,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.857352,
  "lng": -2.251106,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.859181,
  "lng": -2.245721,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.866641,
  "lng": -2.250720,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.872970,
  "lng": -2.249007,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.901819,
  "lng": -2.117134,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.904358,
  "lng": -2.114716,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.905074,
  "lng": -2.122092,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.910953,
  "lng": -2.107387,
  "reporter": "Admin"
}, {
  "hazardType": "Pothole",
  "lat": 51.895359,
  "lng": -2.109824,
  "reporter": "Admin"
}];

$('button').on('click', function() {
  // loop through array dropping markers

  console.log("Are we in the function?");

  for(var i = 0; i < markerList.length; i++) {
    console.log("Are we in the loop");
      var obj = markerList[i];
      var latLng = new google.maps.LatLng(obj.lat, obj.lng);
      var marker = new google.maps.Marker({
        position: latLng,
        title: obj.hazardType,
        animation: google.maps.Animation.DROP,
        map: map
      });
      marker.setMap(map);
  }
  // redraw map

});
