// init any variables
var location;
var startLocation;
var map;
var running = false;
var t,
    seconds = 0,
    minutes = 0,
    hours = 0;

// init map
  function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: { lat: 51.8581438, lng: -2.2518047 }
  });
}

// Add marker
function addMarker(coords) {
    var marker = new google.maps.Marker({
      position: coords,
      animation: google.maps.Animation.DROP,
      map: map
    });
}

// get users location
function getLocation() {
  // settings
  var geoOptions = {
    timeout: 10 * 1000
  };
  // Success callback
  function geoSuccess(position) {
    var pos = position;
    var posLocation = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
    addMarker(posLocation);
    map.panTo(posLocation);

  }
  // Error callback
  function geoError() {
    console.log('Error: ' + error.code);
  }
  // get the position, return it
  var position = navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
  return position;
}

// timer function
function startTimer() {
  t = setTimeout(timeCount, 1000);
}
function timeCount() {
    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
        if (minutes >= 60) {
            minutes = 0;
            hours++;
        }
    }
    var time = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
    $('#timer').html(time);
    startTimer();
}

// Start cycle button event
$('#startCycle').on('click', function() {
  console.log("button clicked");
  startLocation = getLocation();
  $('.logo-popup').fadeOut(500);
  $('.cycle-ui').transition({ y: '0px'}, 750, 'ease');
  $('.cycle-ui').toggleClass('showing');
});

// Minimize/Maximize cycle ui event
$('#view-changer').on('click', function() {
  if ($('.cycle-ui').is('.showing')) {
    $('.cycle-ui').transition({ y: '145px'}, 750, 'ease');
    $('.cycle-ui').toggleClass('showing');
  } else {
    $('.cycle-ui').transition({ y: '0px'}, 750, 'ease');
    $('.cycle-ui').toggleClass('showing');
  }
});

// Start cycle event
$('#toggleCycle').on('click', function() {
  console.log("event started");
  if (running === false) {

    // set running to true
    running = true;
    console.log("function running");

    // Start timer
    setTimeout(startTimer, 1000);
    // Change button text and bg color
    $(this).html("Stop!");
    $(this).transition({ 'background-color': '#0BA8ED'}, 750, 'ease');

    // get location

  } else if (running === true) {

    console.log("function stopped");
    running = false;

    // stop timer
    clearTimeout(t);
    // change button text and bg color
    $(this).html("Start!");
    $(this).transition({ 'background-color': '#01CDD6'}, 750, 'ease');

  }
});

  /* dangers events */
  // get location
  $('.get-location').on('click', getLocation());
  // drop marker array

  var markerList = [{
    "hazardType": "Pothole",
    "lat": 51.862819,
    "lng": -2.237757,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.861248,
    "lng": -2.250484,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.857352,
    "lng": -2.251106,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.859181,
    "lng": -2.245721,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.866641,
    "lng": -2.250720,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.872970,
    "lng": -2.249007,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.901819,
    "lng": -2.117134,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.904358,
    "lng": -2.114716,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.905074,
    "lng": -2.122092,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.910953,
    "lng": -2.107387,
    "reporter": "Admin"
  }, {
    "hazardType": "Pothole",
    "lat": 51.895359,
    "lng": -2.109824,
    "reporter": "Admin"
  }];

$('.drop-markers').on('click', function() {
  // loop through array dropping markers

  console.log("Are we in the function?");

  for(var i = 0; i < markerList.length; i++) {
    console.log("Are we in the loop");
      var obj = markerList[i];
      var latLng = new google.maps.LatLng(obj.lat, obj.lng);
      var marker = new google.maps.Marker({
        position: latLng,
        title: obj.hazardType,
        animation: google.maps.Animation.DROP,
        map: map
      });
      marker.setMap(map);
  }
  // js end
});
